# using lists

Stuff that is available in this repo.

1. display a list of items
2. add new items to the list. 
3. delete an item from the list. 
4. tap on an item to navigate to a details page. 

**references and links**

no specific references that I can talk about. 

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 