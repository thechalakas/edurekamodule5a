//
//  ItemsViewController.swift
//  EdurekaModule5a
//
//  Created by Jay on 15/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

//we are using a TableView so we need this controller to work with it
//also note that this controller also provides the neccessary delegates
//

//alright this is done and ready for class
class ItemsViewController: UITableViewController
{
    //lets get an Item Store reference here.
    //note this is only a reference and not an actual creation of the store.
    //the creation is done with () and is done in the app delegate since we want the items ready the moment the app itself is created.
    var itemStore: ItemStore!
    
    //alright, we need two functions to manipulate the table behavior.
    
    //this one is for adding new item
    @IBAction func addNewItem(_ sender: UIButton)
    {
        print("addNewItem begins")
        
        //lets create a new item
        let newItem = itemStore.createItem()
        
        //get the index of the item.
        let index = itemStore.allItems.index(of: newItem)
        
            //create the entry for the table
        let indexPath = IndexPath(row: index!, section: 0)
            //add the entry into the table view
        tableView.insertRows(at: [indexPath], with: .automatic)
        
        print("addNewItem ends")
    }
    
    //this one is about removing an existing item.
    override func tableView(_ tableView: UITableView,
                            commit editingStyle: UITableViewCellEditingStyle,
                            forRowAt indexPath: IndexPath)
    {
        print("remove item commit editingStyle: UITableViewCellEditingStyle begins")
        //alright lets make sure we are tapped on delete.
        //this is that red dot with minus symbol
        if(editingStyle == .delete)
        {
            //get the item
            let item = itemStore.allItems[indexPath.row]
            //remove it from the collection
            itemStore.removeItem(item)
            //dont forget to remove this from the table view also.
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        print("remove item commit editingStyle: UITableViewCellEditingStyle ends")
    }
    
    //this method is for moving things around.
    override func tableView(_ tableView: UITableView,
                            moveRowAt sourceIndexPath: IndexPath,
                            to destinationIndexPath: IndexPath)
    {
        print("move item moveRowAt sourceIndexPath: IndexPath begins")
        //lets move the elements around in the UI.
        itemStore.moveItem(from: sourceIndexPath.row, to: destinationIndexPath.row)
        print("move item moveRowAt sourceIndexPath: IndexPath ends")
    }

    //this one is for enabling editing mode.
    @IBAction func toggleEditingMode(_ sender: UIButton)
    {
        print("toggleEditingMode begins")

        //if already in editing mode
        //disable it.
        if isEditing
        {
            //change letters so you tell user that you want to edit.
            sender.setTitle("Edit", for: .normal)
            //turn off editing of the table.
            setEditing(false, animated: true)
        }
        else //if not in editing mode enable it
        {
            //change letters to inform that editing is in progress
            sender.setTitle("Done", for: .normal)
            
            //enable the editing
            setEditing(true, animated: true)
        }
        print("toggleEditingMode ends")

    }
    //two things that have to be implemented to get the table working
    
    //set the number of rows.
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        print("tableView numberOfRowsInSection begins")
        
        print("tableView numberOfRowsInSection ends")
        //we just need to inform the table how many rows there will be.
        return itemStore.allItems.count
    }
    
    //set the data for each row.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        print("tableView cellForRowAt begins")
        //we need a UITableViewCell
        //this is the way to create new cells each time you want it.
        //let cell = UITableViewCell(style: .value1, reuseIdentifier: "UITableViewCell")
        
        //this is the way to reuse cells (and of course, create new ones if they were already created)
        //its more of a memory play here.
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        
        //get the item that will be displayed in the row.
        let item = itemStore.allItems[indexPath.row]
        
        //attach things from item to the cell
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = "$\(item.valueInDollars)"
        
        print("tableView cellForRowAt ends")
        
        //return the cell to be displayed in the row
        return cell
    }
    
    //lets set up the inset so that the table will look a little below the top of the screen
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        print("viewDidLoad begins")
        
        //we need the height.
        let heightFromTop = UIApplication.shared.statusBarFrame.height
        
        //Now prepare an inset.
        let insets = UIEdgeInsets(top: heightFromTop, left: 10, bottom: 10, right: 10)
        
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
        
        print("viewDidLoad ends")
    }
    
    //lets take care of that segue and also send that Item to the details view controller
    //this method gets called the moment a segue is triggered
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "showItem")
        {
            print("prepare(for segue - show Item is beginning")
            
            //first collect the row that was selected.
            let row = tableView.indexPathForSelectedRow?.row
            
            //get the item at the row.
            let item = itemStore.allItems[row!]
            
            //get the destination controller reference
            let detailViewController = segue.destination as! DetailViewController
            //set the item collected above to the item in the destination controller
            detailViewController.item = item
        }
    }
    
}














































