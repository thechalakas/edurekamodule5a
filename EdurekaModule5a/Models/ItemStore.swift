//
//  ItemStore.swift
//  EdurekaModule5a
//
//  Created by Jay on 15/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

//no need to use NSObject. we just want an empty class without any inherited properties.
class ItemStore
{
    //we want an array of Items we defined elsewhere.
    var allItems = [Item]()
    
    //we need a method to add items to the above array.
    //discardable means the caller is free to collect or ignore teh return value.
    @discardableResult func createItem() -> Item
    {
        let newItem = Item(random:true)
        
        allItems.append(newItem)
        
        return newItem
    }
    
    //we need a method that will delete items.
    //the opposite of createItem
    func removeItem(_ item: Item)
    {
        //get the index.
        let index = allItems.index(of: item)
        allItems.remove(at: index!)
    }
    
    //we need a method that will move items inside the array
    func moveItem(from fromIndex: Int, to toIndex: Int)
    {
        //if the movment is happening from and to the same location
        if(fromIndex == toIndex)
        {
            //dont do anything.
            return
        }
        
        //first get the item that is being moved
        let movedItem = allItems[fromIndex]
        
        //delete it from the array
        allItems.remove(at: fromIndex)
        
        //insert it back at the new location
        allItems.insert(movedItem, at: toIndex)
    }
    
    init()
    {
        //lets call createItem 5 times 
        for _ in 0..<5
        {
            createItem()
        }
    }
}
