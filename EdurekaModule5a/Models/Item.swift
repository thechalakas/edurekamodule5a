//
//  Item.swift
//  EdurekaModule5a
//
//  Created by Jay on 15/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

//almost everything in Swift is directly or indirectly inherting from NSObject
//its like the father class.
class Item: NSObject
{
    var name: String
    var valueInDollars: Int
    //question mark means optional
    var serialNumber: String?
    let dateCreated: Date
    
    //we need an initializer. give some default values to the above properties.
    //this is the default initializer which is like mandatory.
    init(name: String,
         serialNumber: String?,
         valueInDollars: Int
    )
    {
        //assigning the three value as they arrived as parameters
        self.name = name
        self.valueInDollars = valueInDollars
        self.serialNumber = serialNumber
        //date, collecting it from the Date function
        self.dateCreated = Date()
        
        //call the super init function to close out the definition here.
        super.init()
    }//end of init
    
    //now we will also write what is called as the convenience initializer.
    //this is more like conveniance.
    //if no value is passed, the default value will be false.
    convenience init(random: Bool = false)
    {
        if(random)  //if true
        {
            //lets create some random names, dollar value and serial numbers
            let firstnames = ["Bruce","Clark","Diana","Barry"]
            let lastnames = ["Wayne","Kent","Prince","Allen"]
            
            //get some random number between 0 and 3 as we have 4 items above in the names.
            let randomfirstnamenumber = arc4random_uniform(3)
            let randomlastnamenumber = arc4random_uniform(3)
            
            //we got a randon name here.
            let randomname = firstnames[Int(randomfirstnamenumber)] + " " + lastnames[Int(randomlastnamenumber)]
            
            //we need a random dollar value
            let randomdollarvalue = Int(arc4random_uniform(100))
            
            //we need a random serial value
            let randomserialnumber = firstnames[Int(randomfirstnamenumber)] + String(randomdollarvalue)
            
            //we have all three values. time to initialize.
            self.init(name:randomname,serialNumber:randomserialnumber,valueInDollars:randomdollarvalue)
            
        }
        else
        {
            self.init(name:"",serialNumber:nil,valueInDollars:0)
        }
    }
    
}//end of class Item
