//
//  DetailViewController.swift
//  EdurekaModule5a
//
//  Created by Jay on 16/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

//we are using a TableView so we need this controller to work with it
//also note that this controller also provides the neccessary delegates
//

//alright this is done and ready for class
class DetailViewController: UIViewController
{
    //we need two outlets. one for name and one for dollar.
    @IBOutlet var nameOfHero: UILabel!
    @IBOutlet var dollarValue: UILabel!
    
    //we need an item object to hold the item object that will come from the ItemsViewController
    var item: Item!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("DetailViewController has loaded")
        
        nameOfHero.text = "Hero Name"
        dollarValue.text = String(10)
        
        nameOfHero.text = item.name
        dollarValue.text = String(item.valueInDollars)
    }
}
